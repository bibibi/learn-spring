package com.nguyennv.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TrangChuController {

	@RequestMapping(value = {"/"})
	@ResponseBody
	public String trangchu() {
		return "Hello, I am Nguyen";
	}
}
